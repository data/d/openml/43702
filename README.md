# OpenML dataset: 2016-Global-Ecological-Footprint

https://www.openml.org/d/43702

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The ecological footprint measures the ecological assets that a given population requires to produce the natural resources it consumes (including plant-based food and fiber products, livestock and fish products, timber and other forest products, space for urban infrastructure) and to absorb its waste, especially carbon emissions. The footprint tracks the use of six categories of productive surface areas: cropland, grazing land, fishing grounds, built-up (or urban) land, forest area, and carbon demand on land.
A nations biocapacity represents the productivity of its ecological assets, including cropland, grazing land, forest land, fishing grounds, and built-up land. These areas, especially if left unharvested, can also absorb much of the waste we generate, especially our carbon emissions.
Both the ecological footprint and biocapacity are expressed in global hectares  globally comparable, standardized hectares with world average productivity.
If a populations ecological footprint exceeds the regions biocapacity, that region runs an ecological deficit. Its demand for the goods and services that its land and seas can provide  fruits and vegetables, meat, fish, wood, cotton for clothing, and carbon dioxide absorption  exceeds what the regions ecosystems can renew. A region in ecological deficit meets demand by importing, liquidating its own ecological assets (such as overfishing), and/or emitting carbon dioxide into the atmosphere. If a regions biocapacity exceeds its ecological footprint, it has an ecological reserve.
Acknowledgements
The ecological footprint measure was conceived by Mathis Wackernagel and William Rees at the University of British Columbia. Ecological footprint data was provided by the Global Footprint Network.
Inspiration
Is your country running an ecological deficit, consuming more resources than it can produce per year? Which countries have the greatest ecological deficits or reserves? Do they consume less or produce more than the average country? When will Earth Overshoot Day, the day on the calendar when humanity has used one year of natural resources, occur in 2017?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43702) of an [OpenML dataset](https://www.openml.org/d/43702). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43702/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43702/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43702/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

